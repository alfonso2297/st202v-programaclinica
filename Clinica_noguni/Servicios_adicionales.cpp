/*
 * Servicios_adicionales.cpp
 *
 *  Created on: 14 de jun. de 2017
 *      Author: USUARIO
 */

#include <vector>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string.h>

using namespace std;
struct serv_ad {
    int codigo;
    string nombre;
    int precio;
    string fecha_oferta;
    string descripcion;
};
typedef struct serv_ad serv_ad;
serv_ad aux;
//Usando el aux se aumentar� un registro al archivo
void registrar_act(){
    cout<<"------------ADICIONAR-SERVICIO---------------"<<endl;
    cout<<"Ingrese el codigo de la actividad"<<endl;
    cin>>aux.codigo;
    cout<<"Ingrese el nombre de la actividad"<<endl;
    cin>>aux.nombre;
    cout<<"Ingrese el precio de la actividad"<<endl;
    cin>>aux.precio;
    cout<<"Ingrese la descripcion sobre la actividad"<<endl;
    cin>>aux.descripcion;
}
int main(){
    int opcion;
    string prec,ofer,nombre,fecha_oferta,descripcion,Bcodigo,codigo,precio,Nprecio,Nfecha,linea;
        cout<<"-----------CONFIGURACION-DE-SERVICIOS-ADICIONALES----------"<<endl;
        cout<<"1 Adicionar servicio adicional"<<endl;
        cout<<"2 Modificar un servicio"<<endl;
        cout<<"Ingrese opcion"<<endl;
        cin>>opcion;
        switch(opcion){
            case 1: {
            	ofstream guardar;
            	guardar.open("Servicios_adicionales.csv",ios::app);
                registrar_act();
                guardar<<"\n";
                guardar<<aux.codigo<<";"<<aux.nombre<<";"<<aux.precio<<";;"<<aux.descripcion;
                break;
            }
            case 2: {
                cout<<"---------------MODIFICAR-SERVICIO-ADICIONAL-------------"<<endl;
                cout<<"�Que desea hacer?"<<endl;
                cout<<"1 Modificar precio"<<endl;
                cout<<"2 Crear oferta"<<endl;
                cin>>opcion;
                ifstream leer;
                ofstream temp;
                switch(opcion){
                    case 1: {
                        cout<<"--------------MODIFICANDO-PRECIO--------------"<<endl;
                        leer.open("Servicios_adicionales.csv");
                        temp.open("Temp.csv");
                        bool encontrado=false;
                        cout<<"Ingrese el codigo de la actividad"<<endl;
                        cin>>Bcodigo;
                        while(!leer.eof()){
                            getline(leer,codigo,';');
                            getline(leer,nombre,';');
                            getline(leer,precio,';');
                            getline(leer,fecha_oferta,';');
                            getline(leer,descripcion,'\n');
                            if(codigo==Bcodigo){
                                encontrado=true;
                                cout<<"Codigo de evento: "<<codigo<<endl;
                                cout<<"Nombre del evento: "<<nombre<<endl;
                                cout<<"Precio del evento: "<<precio<<endl;
                                cout<<"Fecha gratuita: "<<fecha_oferta<<endl;
                                cout<<"Descripci�n: "<<descripcion<<endl;
                                cout<<endl;
                                cout<<"Ingrese el nuevo precio "<<endl;
                                cin>>Nprecio;
                                temp<<codigo<<";"<<nombre<<";"<<Nprecio<<";"<<fecha_oferta<<";"<<descripcion<<endl;
                            }else(codigo!="");{
                            temp<<codigo<<";"<<nombre<<";"<<precio<<";"<<fecha_oferta<<";"<<descripcion<<endl;
                            }

                        }
                        if(encontrado==false){
                            cout<<"Codigo no encontrado"<<endl;
                        }
                        break;
                        leer.close();
                        temp.close();
                        break;
                    }
                    case 2: {
                        cout<<"--------------CREANDO-OFERTA----------------"<<endl;
                        leer.open("Servicios_adicionales.csv");
                        temp.open("Temp.csv");
                        bool encontrado=false;
                        cout<<"Ingrese el codigo de la actividad"<<endl;
                        cin>>Bcodigo;
                        while(!leer.eof()){
                            getline(leer,codigo,';');
                            getline(leer,nombre,';');
                            getline(leer,precio,';');
                            getline(leer,fecha_oferta,';');
                            getline(leer,descripcion,'\n');
                            if(codigo==Bcodigo){
                                encontrado=true;
                                cout<<"Codigo de evento: "<<codigo<<endl;
                                cout<<"Nombre del evento: "<<nombre<<endl;
                                cout<<"Precio del evento: "<<precio<<endl;
                                cout<<"Fecha gratuita: "<<fecha_oferta<<endl;
                                cout<<"Descripci�n: "<<descripcion<<endl;
                                cout<<endl;
                                cout<<"Ingrese la fecha de oferta "<<endl;
                                cin>>Nfecha;
                                temp<<codigo<<";"<<nombre<<";"<<precio<<";"<<Nfecha<<";"<<descripcion<<endl;
                            }else(codigo!="");{
                            temp<<codigo<<";"<<nombre<<";"<<precio<<";"<<fecha_oferta<<";"<<descripcion<<endl;
                            }
                        }
                        if(encontrado==false){
                            cout<<"Codigo no encontrado"<<endl;
                        }else{
                            cout<<"Modificaci�n satisfactoria"<<endl;
                        }
                        leer.close();
                        temp.close();
                        break;
                    }
                remove("Servicios_adicionales.csv");
                rename("temp.csv","Servicios_adicionales.csv");
                }
                break;
            }
        }
    }




