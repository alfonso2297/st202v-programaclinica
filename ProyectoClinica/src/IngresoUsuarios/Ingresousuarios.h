/*
 * Ingresousuarios.h
 *
 *  Created on: 28 de jun. de 2017
 *      Author: Alfonso Su�rez
 */

#ifndef INGRESOUSUARIOS_INGRESOUSUARIOS_H_
#define INGRESOUSUARIOS_INGRESOUSUARIOS_H_

#include<bits/stdc++.h>
using namespace std;

struct dato {
	string cod;
	string nomb;
	string apell;
	string nombreU;
	string ruc;
	string dni;
	string tel;
	string sex;
	string fechadeN;
	string correo;
	string cont;
};
typedef struct dato usuario;

string encriptar(string cadena);
string descencriptar (string cad);
void registrar(vector<usuario> u1, unsigned int cont1);
void agregar_usuario(vector<usuario> u1);
int ingresar_usuario(string &codigo);
void PantallaPrincipal();

#endif /* INGRESOUSUARIOS_INGRESOUSUARIOS_H_ */
