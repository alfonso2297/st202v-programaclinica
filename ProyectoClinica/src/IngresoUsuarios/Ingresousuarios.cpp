#include<fstream>
#include<string.h>
#include<cmath>
#include <cstdlib>
#include<vector>
#include<iostream>
#include<stdlib.h>
#include<conio.h>
#include "../menu/menu.h"
using namespace std;
struct dato {
	string cod;
	string nomb;
	string apell;
	string nombreU;
	string ruc;
	string dni;
	string tel;
	string sex;
	string fechadeN;
	string correo;
	string cont;
};
typedef struct dato usuario;

string encriptar(string cadena){
	string alfabeto = "abcdefghijklmnopqrstuvwxyz";
	string clave="hdnoanecloejkcs";
	string encriptar;
	int poscadena,posclave;
	unsigned int tamcadena = cadena.size();
	unsigned int tamclave = clave.size();
	if (tamcadena>tamclave){
		for(unsigned int i=0; i<cadena.size();i++){
			clave+= clave[i];
		}
		for (unsigned int i=0; i<cadena.size();i++){
			poscadena= alfabeto.find(cadena[i]);
			posclave= alfabeto.find(clave[i]);
			encriptar+= alfabeto[(poscadena + posclave)%26];
		}
	}else{
		for (unsigned int i=0; i<cadena.size();i++){
			poscadena= alfabeto.find(cadena[i]);
			posclave= alfabeto.find(clave[i]);
			encriptar+= alfabeto[(poscadena + posclave)%26];
		}
	}
	return encriptar;
}
string desencriptar (string cad){
	string alfabeto = "abcdefghijklmnopqrstuvwxyz";
	string clave="hdnoanecloejkcs";
	string desencriptar;
	unsigned int tamcad = cad.size();
	unsigned int tamclave = clave.size();
	int poscad,posclave, res;
	if (tamcad>tamclave){
		for(unsigned int i=0; i<cad.size();i++){
			clave+= clave[i];
		}
		for (unsigned int i=0; i<cad.size();i++){
			poscad= alfabeto.find(cad[i]);
			posclave= alfabeto.find(clave[i]);
			res= poscad - posclave;
			if (res<0){
				res=26+res;
				desencriptar+=alfabeto[res%26];
			}else{
				desencriptar+= alfabeto[res%26];
			}
		}
	}else{
		for (unsigned int i=0; i<cad.size();i++){
			poscad= alfabeto.find(cad[i]);
			posclave= alfabeto.find(clave[i]);
			res=poscad - posclave;
			if (res<0){
				res=26+res;
				desencriptar+=alfabeto[res%26];
			}else{
				desencriptar+= alfabeto[res%26];
			}
		}
	}
	return desencriptar;
}
void registrar(vector<usuario> u1, unsigned int cont1){
	string pal1;
	string base="0000000";
	char cant[8];
	usuario u;
	cout<<"\t \t  \t \t REGISTRO\n";
	cout<<"\tNombres: ";
	cin.ignore();
	getline(cin,u.nomb);
	cout<<"\tApellidos: ";
	getline(cin,u.apell);
	cout<<"\tNombre de usuario: ";
	getline(cin,u.nombreU);
	cout<<"\tRUC: ";cin>>u.ruc;
	cout<<"\tDNI: ";cin>>u.dni;
	cout<<"\tTelefono: ";cin>>u.tel;
	cout<<"\tSexo: ";cin>>u.sex;
	cout<<"\tFecha de Nacimiento: ";cin>>u.fechadeN;
	cout<<"\tCorreo: ";cin>>u.correo;
	cout<<"\tContrasena: ";cin>>u.cont;
	u.cont= encriptar(u.cont);
	cont1++;
	pal1= itoa(cont1-1,cant,10);
	int j=0;
	for (unsigned int i=base.size()-pal1.size()-1; i<base.size(); i++){
		base[i]=pal1[j];
		j++;
	}
	u.cod="HC"+ base;
	u.cod=u.cod.substr(0,8);
	u1.push_back(u);
	ofstream archivo;
	archivo.open("usuarios.csv",ios::app);
	archivo<<u.cod<<";"<<u.nomb<<";"<<u.apell<<";"<<u.ruc<<";"<<u.dni<<";"<<u.tel<<";"<<u.sex<<";"<<u.fechadeN<<";"<<u.correo<<";"<<u.nombreU<<";"<<u.cont<<endl;
	/*archivo.open("usuarios.csv",ios::trunc);
	archivo<<"Codigo;Nombre;Apellidos;RUC;DNI;Telefono;Sexo;Fecha de nacimiento;Correo;Nombre de Usuario;Contraseña"<<endl;
	for(unsigned int i=0; i<u1.size(); i++){
		archivo<<u1[i].cod<<";"<<u1[i].nomb<<";"<<u1[i].apell<<";"<<u1[i].ruc<<";"<<u1[i].dni<<";"<<u1[i].tel<<";"<<u1[i].sex<<";"<<u1[i].fechadeN<<";"<<u1[i].correo<<";"<<u1[i].nombreU<<";"<<u1[i].cont<<endl;
	}*/
	archivo.close();
	cout<<endl;
	cout<<"***Usuario registrado correctamente***"<<endl;
}
void agregar_usuario(vector<usuario> u1){
	ifstream archivo1;
	archivo1.open("usuarios.csv",ios::in);
	usuario aux;
	string linea;
	int cont=0;
	if ( archivo1.is_open()){
		while (!archivo1.eof()){
			getline(archivo1,linea);
			if (cont>=1){
				unsigned int j=0;
				int cont1=0;
				while(j<linea.size()){
					while(linea[j]==';' && j<linea.size()){
						j++;
					}
					string pal="";
					while(linea[j]!=';'  && j<linea.size()){
						pal+=linea[j];
						j++;
					}
					if (linea.size()>0){
						cont1++;
						if (cont1==1){
							aux.cod=pal;
						}
						if (cont1==2){
							aux.nomb=pal;
						}
						if (cont1==3){
							aux.apell=pal;
						}
						if (cont1==4){
							aux.ruc=pal;
						}
						if (cont1==5){
							aux.dni=pal;
						}
						if (cont1==6){
							aux.tel=pal;
						}
						if (cont1==7){
							aux.sex=pal;
						}
						if (cont1==8){
							aux.fechadeN=pal;
						}	
						if (cont1==9){
							aux.correo=pal;
						}
						if (cont1==10){
							aux.nombreU=pal;
						}
						if (cont1==11){
							aux.cont=pal;
						}
					}
				}
				u1.push_back(aux);
			}
			cont++;
		}
	}
	unsigned int cont3=u1.size();
	registrar(u1,cont3);
	archivo1.close();
}

int ingresar_usuario(string &codigo){
	ifstream archivo1;
	archivo1.open("usuarios.csv",ios::in);
	vector<usuario> u2;
	usuario aux;
	string linea;
	int cont=0;
	if (archivo1.is_open()){
		while (!archivo1.eof()){
			getline(archivo1,linea);
			if (cont>=1){
				unsigned int j=0;
				int cont1=0;
				while(j<linea.size()){
					while(linea[j]==';' && j<linea.size()){
						j++;
					}
					string pal="";
					while(linea[j]!=';'  && j<linea.size()){
						pal+=linea[j];
						j++;
					}
					if (linea.size()>0){
						cont1++;
						if (cont1==1){
							aux.cod=pal;
						}
						if (cont1==2){
							aux.nomb=pal;
						}
						if (cont1==3){
							aux.apell=pal;
						}
						if (cont1==4){
							aux.ruc=pal;
						}
						if (cont1==5){
							aux.dni=pal;
						}
						if (cont1==6){
							aux.tel=pal;
						}
						if (cont1==7){
							aux.sex=pal;
						}
						if (cont1==8){
							aux.fechadeN=pal;
						}	
						if (cont1==9){
							aux.correo=pal;
						}
						if (cont1==10){
							aux.nombreU=pal;
						}
						if (cont1==11){
							aux.cont=pal;
						}
					}
				}
				u2.push_back(aux);
			}
			cont++;
		}
	}
	string adm="administrador";
	string cont_adm="administrador";
	string secr="secretario";
	string cont_secr="secretario";
	string pass;
	string usu,contr;
	int n;
	bool r=0;
	cout<<"\t \t  \t   \t BIENVENIDO\n";
	do {
		cout<<"\tUsuario: ";cin>>usu;
		cout<<"\tContrasena: ";cin>>contr;
		for(unsigned int i=0; i<u2.size();i++){
			pass=desencriptar(u2[i].cont);
			if (contr==pass && usu==u2[i].nombreU){
				codigo=u2[i].cod;
				r = !r;
				n=1;
				i=u2.size();
			}
		}
		if(usu==adm && contr==cont_adm){
			r= !r;
			n=2;
		}
		if(usu==secr && contr==cont_secr){
			r= !r;
			n=3;
		}
		if (r==0){
			cout<<"\n\tEl nombre de usuario o la contrasena fueron mal digitados. Ingrese sus datos nuevamente."<<endl;
		}
	}while(r==0);
	archivo1.close();
	return n;
}
void PantallaPrincipal(){
	vector<usuario> U;
	string codigo;
	char opcion;
	int nivel;
	do {
		cout<<"\n\t1.Registrarse"<<endl;
		cout<<"\t2.Iniciar sesion"<<endl;
		cout<<"\t3.Salir"<<endl;
		cout<<"\tOPCION: ";
		cin>>opcion;
		switch (opcion){
		case'1':		
			agregar_usuario(U);
			break;
		case '2':
			nivel=ingresar_usuario(codigo);	
			TipoMenu(nivel,codigo);
			break; 
		}
	}while(opcion!='3');
}
