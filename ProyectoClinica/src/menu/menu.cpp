/*
 * menu.cpp
 *
 *  Created on: 27 de jun. de 2017
 *      Author: Alfonso Su�rez
 */
#include <bits/stdc++.h>
using namespace std;
#include "../Citas/citas.h"
#include "../ServiciosAdicionales/Servicios_adicionales.h"
#include "../medicamentos/admi.h"
#include "../medicamentos/cliente.h"
#include "../ExternoPorEmergencia/Externo_Por_Emergencia.h"

void TipoMenu(int nivel,string id)
{
	//cout<<"NIVEL: "<<nivel<<endl;
	//cout<<"ID: " <<id<<endl;
	int r=0;
		if (nivel==1) //cliente
		{
			while(r!=4)
			{
				system("cls");
				cout<<"\t*******OPCIONES DISPONIBLES PARA USTED*****"<<endl;
				cout<<"\t*******************************************\n"<<endl;
				cout<<"1 : Agenda tu cita medica"<<endl;
				cout<<"2 : Consulta y obten tus medicamentos"<<endl;
				cout<<"3 : Consulta los servicios adicionales que tenemos para ti"<<endl;
				cout<<"4 : Cerrar Sesion"<<endl;
		        cout<<"\n\tRespuesta: ";
				cin>>r;
				if (r!=1 && r!=2 && r!=3 && r!=4)
				{
					cout<<"Ingrese una opcion valida"<<endl;
				} else
				{
					switch(r)
					{
					case 1:RegistrarCitas(id);break;
					case 2:cliente(); break;
					case 3: admin_mostrar_servad(); break;
					case 4:system("cls"); break;
					}
				}
			}
		}



		if (nivel==2) //administrador
				{
					while(r!=5)
					{
						system("cls");
						cout<<"\t*******************************************\n"<<endl;
						cout<<"\t*******OPCIONES DISPONIBLES PARA USTED*****"<<endl;
						cout<<"\t*******************************************\n"<<endl;
						cout<<"1 : Mostrar consultas pendientes"<<endl;
						cout<<"2 : Mostrar o modificar los medicamentos en existencias"<<endl;
						cout<<"3 : Agregar o modificar servicios adicionales"<<endl;
						cout<<"4 : Registrar o mostrar pacientes en emergencia"<<endl;
						cout<<"5 : Cerrar sesion"<<endl;
				        cout<<"\n\tRespuesta: ";
						cin>>r;
						if (r!=1 && r!=2 && r!=3 && r!=4 && r!=5)
						{
							cout<<"Ingrese una opcion valida"<<endl;
						} else
						{
							switch(r)
							{
							case 1:MostarCitas();break;
							case 2:admi();break;
							case 3:admin_config_servad();break;
							case 4:paciente();break;
							case 5:system("cls"); break;
							}
						}
					}
				}


		if (nivel==3) //secretario
					{
						while(r!=5)
						{
							system("cls");
							cout<<"\t*******************************************\n"<<endl;
							cout<<"\t*******OPCIONES DISPONIBLES PARA USTED*****"<<endl;
							cout<<"\t*******************************************\n"<<endl;
							cout<<"1 : Mostrar consultas pendientes"<<endl;
							//cout<<"2 : Mostrar medicamentos en existencias"<<endl;
							cout<<"2 : Mostrar servicios adicionales"<<endl;
							cout<<"3 : Registrar o mostrar pacientes en emergencia"<<endl;
							cout<<"4 : Cerrar sesion"<<endl;
						    cout<<"\n\tRespuesta: ";
							cin>>r;
							if (r!=1 && r!=2 && r!=3 && r!=4 && r!=5)
							{
								cout<<"Ingrese una opcion valida"<<endl;
							} else
							{
								switch(r)
								{
								case 1:MostarCitas();break;

								case 2:admin_mostrar_servad(); break;
								case 3:paciente();break;
								case 4:system("cls"); break;
								}
							}
						}
					}

}



