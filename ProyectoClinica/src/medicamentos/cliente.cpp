#include<iostream>
#include<stdlib.h>
#include<conio.h>
#include<fstream>
#include<vector>
#include<string.h>
#include<cmath>
#include<sstream>

using namespace std;
struct farmacia{
	string id;
	string nombre;
	string descripcion;
	string precio;
	string alertas;
	string envases;
	string stock;
};
typedef struct farmacia far;
vector<far> vector1;
void menu(){
	cout<<"\n ----------------------------------------------------------------"<<endl;
	cout<<"                  BIENVENIDO  A  LA FARMACIA                 "<<endl;
	cout<<" ----------------------------------------------------------------"<<endl;
	cout<<"|   A._Buscar un medicamento                                     |"<<endl;
	cout<<"|   B._Salir                                                     |"<<endl;
	cout<<" ----------------------------------------------------------------"<<endl;
}
string convertir(int num){
	stringstream stream;
	stream<<num;
	return stream.str();
}
void banco(int k,vector<far> vector1,int rpt){
	int num,n;
	long long int tar;
	cout<<"\n----------------------------------------------------------------"<<endl;
	cout<<"                         COMPRA DE MEDICAMENTOS                    "<<endl;
	cout<<"----------------------------------------------------------------"<<endl;
	cout<<"Ingrese su numero de tarjeta de credito : ";
	cin>>tar;
	cout<<"Ingrese su contrasenia : ";
	long long int contra;
	cin>>contra;
	if(201700001<=tar && tar<=201700020){
		if(201700001<=contra && contra<=201700020){
			cout<<"\n______________________________________________"<<endl;
			n=atoi(vector1[k].stock.c_str());
			num=n;
			vector1[k].stock=convertir(num);
			cout<<"\nEl nuevo stock es : "<<atoi(vector1[k].stock.c_str())-rpt<<endl;
			cout<<"_______________________________________________"<<endl;
		}	
	}else{
		cout<<"***   EL NUMERO DE TARJETA Y/O LA CONTRASE�A NO ESTAN ASOCIADAS A NINGUNA CUENTA   ***"<<endl;
	}	
}
void menu_opcion(){
	cout<<"\n----------------------------------------------------------------"<<endl;
	cout<<"               �QUE DESEA HACER A CONTINUACION?                 "<<endl;
	cout<<" ----------------------------------------------------------------"<<endl;
	cout<<"|   A._Regresar al menu de opciones                              |"<<endl;
	cout<<"|   B._Salir                                                     |"<<endl;
	cout<<" ----------------------------------------------------------------"<<endl;
}
void menu_compra(){
	char rta;
	int llave1;
	menu_opcion();
	cout<<"\nSelecione la opcion que desea realizar : ";
	cin>>rta;
	rta=toupper(rta);
	if(rta=='A'){
		llave1=1;
	}else{
		if(rta=='B'){
			return ;
		}else{
			cout<<"\n---------ERROR OPCION ELEGIDA ES INCORRECTA, INTENTELO NUEVAMENTE-----------"<<endl;
		}
	}
}
void menu_medicamentos(){
	cout<<"\n ---------------------------------------------------------------"<<endl;
	cout<<"                   ELIJA UNA OPCION              "<<endl;
	cout<<" ----------------------------------------------------------------"<<endl;
	cout<<"|   A._Ver la descripcion                                       |"<<endl;
	cout<<"|   B._Ver las presentaciones                                   |"<<endl;
	cout<<"|   C._Ver las advertencias                                     |"<<endl;
	cout<<"|   D._Ver el precio                                            |"<<endl;
	cout<<"|   E._Regresar al menu de busqueda                             |"<<endl;
	cout<<" ----------------------------------------------------------------"<<endl;
}
void descrip(int i, vector<far> vector){
	cout<<"\n_______________________________________________"<<endl;
	cout<<"La descripcion del medicamento es : "<<endl;
	cout<<vector[i].descripcion<<endl;
	cout<<"_______________________________________________"<<endl;
}
void present(int i, vector<far> vector){
	cout<<"\n_______________________________________________"<<endl;
	cout<<"La presentacion del medicamento es : "<<endl;
	cout<<vector[i].envases<<endl;
	cout<<"_______________________________________________"<<endl;
}
void adver(int i, vector<far> vector){
	cout<<"\n___________________________________________________________________________________________________________________________________________________"<<endl;
	cout<<"Las advertencias del medicamento son : "<<endl;
	cout<<vector[i].alertas<<endl;
	cout<<"____________________________________________________________________________________________________________________________________________________"<<endl;
}
void precio(int i, vector<far> vector){
	cout<<"\n______________________________________________"<<endl;
	cout<<"El precio del medicamento es : ";
	cout<<vector[i].precio<<endl;
	string res;
	int llave,cant;
	cout<<"\nDesea comprar el medicamento [si/no] : ";
	cin>>res;
	cout<<"_______________________________________________"<<endl;
	if(res=="si"){
		cout<<"\n�Cuantas unidades desea? : ";
		cin>>cant;
		banco(i,vector,cant);
		cout<<"========================================================================="<<endl;
		cout<<"                          RECIBO DE COMPRA"<<endl;
		cout<<"========================================================================="<<endl;
		cout<<"\nEl medicamento adquirido es : "<<vector[i].nombre<<endl;
		cout<<"El codigo del medicamento es : "<<vector[i].id<<endl;
		if(cant==1){
			cout<<"El numero de medicamentos adquiridos es : "<<cant<<" unidad"<<endl;
		}else{
			cout<<"El numero de medicamentos adquiridos es : "<<cant<<" unidades"<<endl;
		}
		cout<<"El pago efectuado por el medicamento es :"<<cant*atoi(vector[i].precio.c_str())<<endl;
		cout<<"\nGracias por su comprar acercarse a la ventanilla a recoger su producto "<<endl;
		cout<<"\n========================================================================="<<endl;
		menu_compra();
	}else{
		if(res=="no"){
			llave=1;
		}else{
			cout<<"\n---------ERROR OPCION ELEGIDA ES INCORRECTA, INTENTELO NUEVAMENTE-----------"<<endl;
		}
	}
	
}
void buscar(){
	ifstream archivo;
	archivo.open("medicamentos.csv",ios::in);
	vector<far> str;
	far med;
	string dato;
	int lim=0;
	if(archivo.fail()){
		cout<<"error al abrir el archivo"<<endl;
		exit(1);
	}
	while (!archivo.eof()){
		getline(archivo,dato);
		if (lim>=1){
			unsigned int j=0;
			int cont=0;
			while(j<dato.size()){
				while(j<dato.size() && dato[j]==';'){
					j=j+1;
				}
				string texto="";
				while(j<dato.size() && dato[j]!=';'){
					texto=texto+dato[j];
					j=j+1;
				}
				if (dato.size()>0){
					cont=cont+1;
					if (cont==1){
						med.id=texto;
					}
					if (cont==2){
						med.nombre=texto;
					}
					if (cont==3){
						med.descripcion=texto;
					}
					if (cont==4){
						med.precio=texto;
					}
					if (cont==5){
						med.alertas=texto;
					}
					if (cont==6){
						med.envases=texto;
					}
					if (cont==7){
						med.stock=texto;
					}
				}
			}
			str.push_back(med);
		}
		lim=lim+1;
	}
	long long int cod;
	int max,min;
	for(int i=0;i<str.size();i++){
		if(i==0){
			min=atoi(str[i].id.c_str());
		}else{
			if(atoi(str[i].id.c_str())<=min){
				min=atoi(str[i].id.c_str());
			}
		}
	}
	bool llave=false;	
	do{
		cout<<"\nIngrese el codigo del medicamento  a buscar : ";
		cin>>cod;
		int num,d,num_cifras;
		num=min;
		while(num>0){
			d=num%10;
			num_cifras=num_cifras+1;
			num=int(num/10);
		}
		if(cod<pow(10,num_cifras)){
			if(cod>=min){
				for(unsigned int i=0;i<str.size();i++){
					if(atoi(str[i].id.c_str())==cod){		
						cout<<"\nEl medicamento "<<str[i].nombre<<" esta disponible"<<endl;
						cout<<"El stock que tenemos es : "<<str[i].stock<<endl;
						char rpta;
						do{
							menu_medicamentos();
							cout<<"\nSelecione la opcion que desea realizar : ";
							cin>>rpta;
							rpta=toupper(rpta);
							switch(rpta){
								case 'A':descrip(i,str);
									break;
								case 'B':present(i,str);
									break;
								case 'C':adver(i,str);
									break;
								case 'D':precio(i,str);
									break;
								case 'E':return ;
									break;
								default :cout<<"---------ERROR OPCION ELEGIDA ES INCORRECTA-----------"<<endl;
									break;
							} 
						}while(rpta!='A' || rpta!='B' || rpta!='C' || rpta!='D');
					}	
				}
				cout<<"\nEn estos momentos no contamos con ese producto "<<endl;
				llave=true;
			}else{
				cout<<"***   ERROR CODIGO INGRESADO ES INCORRECTO   ***"<<endl;
				llave=true;
			}
		}else{
			cout<<"***   ERROR CODIGO INGRESADO SUPERA LA CANTIDAD DE DIGITOS VALIDOS  ***"<<endl;
			llave=true;
		}	
	}while(llave==true);
	archivo.close();
}
void cliente(){
	char rpta;
	do{
		menu();
		cout<<"\nSelecione la opcion que desea realizar : ";
		cin>>rpta;
		if(rpta>='A' && rpta<='z'){
			rpta=toupper(rpta);
			switch(rpta){
				case 'A':buscar();
					break;
				case 'B':return ;
					break;
				default :cout<<"\n---------ERROR OPCION ELEGIDA ES INCORRECTA-----------"<<endl;
					break;
			} 
		}else{
			cout<<"\n***    ERROR VALOR DIGITADO ES INCORRECTO   ***"<<endl;
		}
	}while(rpta!='A' || rpta!='B');
}
int cliente2(){
	cliente();
	return 0;
}
