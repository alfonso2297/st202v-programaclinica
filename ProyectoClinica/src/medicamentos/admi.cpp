#include<iostream>
#include<stdlib.h>
#include<conio.h>
#include<fstream>
#include<vector>
#include<string.h>
#include<sstream>
using namespace std;
struct farmacia{
	string id;
	string nombre;
	string descripcion;
	string precio;
	string alertas;
	string envases;
	string stock;
};
typedef struct farmacia far;

void menu_admi(){
	cout<<"\n---------------------------------------------------------------------------"<<endl;
	cout<<"                              BIENVENIDO"<<endl;
	cout<<"-----------------------------------------------------------------------------"<<endl;
	cout<<" A._ Ver un medicamento"<<endl;
	cout<<" b._ Ver la lista de medicamentos"<<endl;
	cout<<" C._ Agregar nuevos medicamentos"<<endl;
	cout<<" D._ Salir"<<endl;
	cout<<"-----------------------------------------------------------------------------"<<endl;
}
void med(){
	ifstream archivo;
	archivo.open("medicamentos/medicamentos.csv",ios::in);
	vector<far> str;
	far med;
	string dato;
	int lim=0;
	if(archivo.fail()){
		cout<<"error al abrir el archivo"<<endl;
		exit(1);
	}
	while (!archivo.eof()){
		getline(archivo,dato);
		if (lim>=1){
			unsigned int j=0;
			int cont=0;
			while(j<dato.size()){
				while(j<dato.size() && dato[j]==';'){
					j=j+1;
				}
				string texto="";
				while(j<dato.size() && dato[j]!=';'){
					texto=texto+dato[j];
					j=j+1;
				}
				if (dato.size()>0){
					cont=cont+1;
					if (cont==1){
						med.id=texto;
					}
					if (cont==2){
						med.nombre=texto;
					}
					if (cont==3){
						med.descripcion=texto;
					}
					if (cont==4){
						med.precio=texto;
					}
					if (cont==5){
						med.alertas=texto;
					}
					if (cont==6){
						med.envases=texto;
					}
					if (cont==7){
						med.stock=texto;
					}
				}
			}
			str.push_back(med);
		}
		lim=lim+1;
	}
	int n;
	for(unsigned int i=0;i<str.size();i++){
		do{
			cout<<"\nDigite el ID del medicamento : ";
			cin>>n;
		}while(n<201700001);
		if(n==atoi(str[i].id.c_str())){
			cout<<"-----------------------------------------------------------------------------"<<endl;
			cout<<"                       INFORMACION DEL MEDICAMENTO"<<endl;
			cout<<"-----------------------------------------------------------------------------"<<endl;
			cout<<"El ID del medicamento es :"<<str[i].id<<endl;
			cout<<"El nombre del medicamento es : "<<str[i].nombre<<endl;
			cout<<"El precio del medicamento es : "<<str[i].precio<<endl;
			cout<<"el stock del medicamento es : "<<str[i].stock<<endl;
			cout<<"-----------------------------------------------------------------------------"<<endl;
		}
	}
}
void lista(vector<far> str){
	ofstream archivo;
	archivo.open("medicamentos/lista_medicamentos.csv",ios::out);
	if(archivo.fail()){
		cout<<"\nerror al crear el archivo"<<endl;
		exit(1);
	}
	archivo<<"ID ; NOMBRE ; PRECIO ; STOCK "<<endl;
	for(unsigned int i=0;i<str.size();i++){
		archivo<<str[i].id<<" ; "<<str[i].nombre<<" ; "<<str[i].precio<<" ; "<<str[i].stock<<endl;
	}
	archivo.close();
	cout<<"\nSe a creado un archivo : ListaMedicamentos.csv que contiene la lista de los medicamentos y su informacion respectiva"<<endl;
}
void leer(vector<far> str){
	ifstream archivo;
	archivo.open("medicamentos/medicamentos.csv",ios::in);
	far med;
	string dato;
	int lim=0;
	if(archivo.fail()){
		cout<<"error al abrir el archivo"<<endl;
		exit(1);
	}
	while (!archivo.eof()){
		getline(archivo,dato);
		if (lim>=1){
			unsigned int j=0;
			int cont=0;
			while(j<dato.size()){
				while(j<dato.size() && dato[j]==';'){
					j=j+1;
				}
				string texto="";
				while(j<dato.size() && dato[j]!=';'){
					texto=texto+dato[j];
					j=j+1;
				}
				if (dato.size()>0){
					cont=cont+1;
					if (cont==1){
						med.id=texto;
					}
					if (cont==2){
						med.nombre=texto;
					}
					if (cont==3){
						med.descripcion=texto;
					}
					if (cont==4){
						med.precio=texto;
					}
					if (cont==5){
						med.alertas=texto;
					}
					if (cont==6){
						med.envases=texto;
					}
					if (cont==7){
						med.stock=texto;
					}
				}
			}
			str.push_back(med);
		}
		lim=lim+1;
	}
	archivo.close();
	lista(str);
}
void agregar(vector<far> str){
	far a;
	int n;	
	cout<<"¿Cuantos medicamentos se adicionaran? : ";
	cin>>n;
	ofstream archivo;
	archivo.open("medicamentos/medicamentos.csv",ios::app);
	if(archivo.fail()){
		cout<<"error al abrir el archivo"<<endl;
		exit(1);
	}
	for(unsigned int i=0;i<n;i++){
		cout<<"\n-------------------------------------Nuevo medicamento("<<i+1<<")----------------------------------------"<<endl;
		cout<<"\nID del medicamento es : ";
		cin>>a.id;
		cout<<"Nombre del medicamento es : ";
		cin>>a.nombre;
		cout<<"Descripcion del medicamento es : ";
		cin>>a.descripcion;
		cout<<"Precio del medicamento es : ";
		cin>>a.precio;
		cout<<"Advertencias del medicamento : ";
		cin>>a.alertas;
		cout<<"Envase del medicamento es : ";
		cin>>a.envases;
		cout<<"Stock del medicamento es : ";
		cin>>a.stock;
		str.push_back(a);
		cout<<"-------------------------------------------------------------------------------------------------"<<endl;
		archivo<<" ; "<<str[i].id<<" ; "<<str[i].nombre<<" ; "<<str[i].descripcion<<" ; "<<str[i].precio<<" ; "<<str[i].alertas<<" ; "<<str[i].envases<<" ; "<<str[i].stock;	
	}
	archivo.close();
}
void admi(){
	vector<far> str;
	char rpta;
	do{
		menu_admi();
		cout<<"\nSelecione la opcion que desea realizar"<<endl;
		cin>>rpta;
		rpta=toupper(rpta);
		switch(rpta){
			case 'A':med();
				break;
			case 'B':leer(str);
				break;
			case 'C':agregar(str);
				break;
			case 'D':exit(1);
				break;
			default :cout<<"---------ERROR OPCION ELEGIDA ES INCORRECTA-----------"<<endl;
				break;
		} 
	}while(rpta!='A' || rpta!='B' || rpta!='C' || rpta!='D');
}
//int admiMedicamentos(){
	//admi();
	//return 0;
//}
