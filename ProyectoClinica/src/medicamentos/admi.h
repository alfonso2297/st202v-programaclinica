/*
 * admi.h
 *
 *  Created on: 28 de jun. de 2017
 *      Author: Alfonso Su�rez
 */

#ifndef MEDICAMENTOS_ADMI_H_
#define MEDICAMENTOS_ADMI_H_
#include<iostream>
#include<stdlib.h>
#include<conio.h>
#include<fstream>
#include<vector>
#include<string.h>
#include<sstream>
using namespace std;
struct farmacia{
	string id;
	string nombre;
	string descripcion;
	string precio;
	string alertas;
	string envases;
	string stock;
};
typedef struct farmacia far;



void menu_admi();
void med();
void lista(vector<far> str);
void leer(vector<far> str);
void agregar(vector<far> str);
void admi();






#endif /* MEDICAMENTOS_ADMI_H_ */
