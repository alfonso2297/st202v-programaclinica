/*
 * cliente.h
 *
 *  Created on: 28 de jun. de 2017
 *      Author: Alfonso Su�rez
 */

#ifndef MEDICAMENTOS_CLIENTE_H_
#define MEDICAMENTOS_CLIENTE_H_

#include<iostream>
#include<stdlib.h>
#include<conio.h>
#include<fstream>
#include<vector>
#include<string.h>
#include<cmath>
#include<sstream>
using namespace std;

typedef struct farmacia far;

void menu();
string convertir(int num);
void compra(int i, vector<far> vector);
void menu_opcion();
void menu_compra();
void menu_medicamentos();
void descrip(int i, vector<far> vector);
void present(int i, vector<far> vector);
void adver(int i, vector<far> vector);
void precio(int i, vector<far> vector);
void buscar();
void cliente();
int cliente2();


#endif /* MEDICAMENTOS_CLIENTE_H_ */
