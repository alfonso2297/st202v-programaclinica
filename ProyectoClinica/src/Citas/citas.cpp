/*
 * citas.cpp
 *
 *  Created on: 20 de jun. de 2017
 *      Author: Alfonso Su�rez
 */
#include <bits/stdc++.h>
using namespace std;



void RegistrarCitas(string id);
void MostarCitas();

/*int main()
{
    //INVOCADOR
    string id="HC000001";
    //RegistrarCitas(id);
    MostarCitas();

}*/

vector<string>Horarios;

/**************DESARROLLO DE FUNCIONES****************/
void RegistrarCitas(string id)
{
    string sede,esp,doc;
    string lineatemp,especialidadstring;
    vector <string> especialidad;
    vector <string> doctor;
    cout<<"---------------------------------"<<endl;
    cout<<"*********RESERVE SU CITA*********"<<endl;
    cout<<"---------------------------------"<<endl;
    cout<<"   INGRESE LA SEDE"<<endl;
    int rp1=0;
    do
    {
        cout<<"\t1: LIMA"<<endl;
        cout<<"\t2: SAN BORJA"<<endl;
        cout<<"\t3: PIURA"<<endl;
        cout<<"\t4: VOLVER"<<endl;
        cout<<"\n\tRespuesta: ";
        cin>>rp1;
    }while(rp1!=1 && rp1!=2 && rp1!=3 && rp1!=4);
    bool gt=true;
    switch (rp1)
    {
        case 1: sede="Lima"; break;
        case 2: sede="San Borja"; break;
        case 3: sede="Piura"; break;
        case 4: gt=false; break;
    }
    if(gt){
    /***ESPECIALIDADES****/
    ifstream ReadDoc("ArchivosCitas/SedesEspecialidadesDoctores.csv");
    int v=0;
    getline(ReadDoc,lineatemp);
    bool rep=false;
    while(getline(ReadDoc,lineatemp))
        {
            if (sede==lineatemp.substr(0,sede.size()))
            {
                /***LINEAS SEDE****/
                lineatemp=lineatemp.substr(sede.size()+1);
                //cout<<lineatemp<<endl;
                especialidadstring="";
                for( int i=0 ; i<lineatemp.size() ; i++)
                {
                    if (lineatemp[i]==';')
                    {
                        i=lineatemp.size();
                        for(int i=0 ; i<especialidad.size() ; i++)
                        {
                            if(especialidad[i]==especialidadstring)
                            {
                                rep=true;
                            } else rep=false;

                        }
                        if(rep==false)
                        {
                            especialidad.push_back(especialidadstring);
                        }



                    }else especialidadstring=especialidadstring+lineatemp[i];
                }
            }
        }
        ReadDoc.close();
    system("cls");
    cout<<"---------------------------------"<<endl;
    cout<<"*********RESERVE SU CITA*********"<<endl;
    cout<<"---------------------------------"<<endl;
    cout<<"   INGRESE LA ESPECIALIDAD"<<endl;
    for(int i=0 ; i<especialidad.size(); i++)
    {
        cout<<"\t"<<i+1<<": "<<especialidad[i]<<endl;
    }
    //cout<<especialidad.size()+1<<": VOLVER"<<endl;
    cout<<"\n\tRespuesta: ";
    cin>>v;
    esp=especialidad[v-1];
    cout<<esp;

    string doctorstring;
    /***DOCTORES****/
    ifstream ReadDoc1("ArchivosCitas/SedesEspecialidadesDoctores.csv");
    v=0;
    getline(ReadDoc1,lineatemp);
    rep=false;
    while(getline(ReadDoc1,lineatemp))
        {
            if (esp==lineatemp.substr(sede.size()+1,esp.size()))
            {
                /***LINEAS SEDE****/
                lineatemp=lineatemp.substr(sede.size()+1+esp.size()+1);
                //cout<<lineatemp<<endl;
                doctorstring="";
                for( int i=0 ; i<lineatemp.size() ; i++)
                {
                    if (lineatemp[i]==';')
                    {
                        i=lineatemp.size();
                        for(int i=0 ; i<doctor.size() ; i++)
                        {
                            if(doctor[i]==doctorstring)
                            {
                                rep=true;
                            } else rep=false;

                        }
                        if(rep==false)
                        {
                            doctor.push_back(doctorstring);
                        }



                    }else doctorstring=doctorstring+lineatemp[i];
                }
            }
        }
    system("cls");
    cout<<"---------------------------------"<<endl;
    cout<<"*********RESERVE SU CITA*********"<<endl;
    cout<<"---------------------------------"<<endl;
    cout<<"   SELECCIONE EL MEDICO"<<endl;
    //cout<<"SIZE de DOCTOR es: " << doctor.size()<<endl;
    for(int i=0 ; i<doctor.size(); i++)
    {
        cout<<"\t"<<i+1<<": "<<doctor[i]<<endl;
    }
    //cout<<doctor.size()+1<<": VOLVER"<<endl;
    cout<<"\n\tRespuesta: ";
    cin>>v;
    doc=doctor[v-1];
    //cout<<doc;

    string superstring=sede+";"+esp+";"+doc+";";

    /***SOLICITANDO FECHA PARA LA CITA***/
    string fechacita;
    string cod;
    system("cls");
    cout<<"---------------------------------"<<endl;
    cout<<"*********RESERVE SU CITA*********"<<endl;
    cout<<"---------------------------------"<<endl;
    cout<<"   INGRESE LA FECHA A TRATARSE (dd/mm)"<<endl;
    cout<<"\n\tRespuesta: ";
    cin>>fechacita;

    //cout<<superstring<<endl;

    ifstream ReadDoc2("ArchivosCitas/SedesEspecialidadesDoctores.csv");
    getline(ReadDoc2,lineatemp);
    while(getline(ReadDoc2,lineatemp))
    {
        //cout<<lineatemp.substr(0,superstring.size())<<endl;
        if (lineatemp.substr(0,superstring.size())==superstring)
        {
            //cout<<"entrando"<<endl;
            cod=lineatemp.substr(superstring.size(),lineatemp.size()-superstring.size());
        }
    }
    ReadDoc2.close();
    //cout<<cod<<endl;

    /******YA TENGO CODIGO (COD)Y FECHA(FECHACITA)*******/
    system("cls");
    vector<string> bd;
    /*v=0;*/ string respaldo="";
    ifstream ReadCitas2("ArchivosCitas/Citas.csv");
    int posicion=0; v=0;
    while(getline(ReadCitas2,lineatemp))
    {
        //cout<<lineatemp.substr(5,5)<<endl;
        v++;
        if (lineatemp.substr(5,5)==fechacita)
        {
            //CADENA A MODIFICAR
            bd.push_back(respaldo);
            respaldo=lineatemp; /*****RESPALDO TIENE LA CADENA A MIDIFICAR***/
            posicion=v;

        } else
        {
            bd.push_back(lineatemp);
        }
    }
    int posdoc;
    posdoc=(atoi(cod.substr(3,5).c_str())-1)*92+11;
//APOYO
    vector<string>Horarios2;
    for(int i=posdoc+1 ; i<posdoc+90; i=i+9)
    {
        Horarios2.push_back(respaldo.substr(i,8));
    }


    vector<bool>permiso;
    cout<<"--------"<<endl;
    cout<<"HORARIOS SEDE: "<<sede<<", ESPECIALIDAD: "<<esp<<", DOCTOR(A): "<<doc<<", FECHA: "<<fechacita<<"/2017"<<endl;
    cout<<"--------"<<endl;
    /*for(int i=0 ; i<Horarios2.size();i++)
    {
        cout<<Horarios2[i]<<endl;
    }*/
    v=0;
    float horas=8.00;
    for(int i=0 ; i<Horarios2.size() ; i++ )
    {
        cout<<fixed << setprecision(2) << "\t"<<i+1<<": "<<"\t"<<horas<<" - "<<horas+1.00<<" horas: " ;
        horas=horas+1.00;
        if (Horarios2[i]=="11111111")
        {
            cout<<"\tDISPONIBLE"<<endl;
            permiso.push_back(true);
        }else if (Horarios2[i]=="22222222")
        {
            cout<<"\tNO DISPONIBLE"<<endl;
            permiso.push_back(false);
        }else
        {
            cout<<"\tNO DISPONIBLE"<<endl;
            permiso.push_back(false);
        }
    }
    do{
    v++;
    if(v>1)
    {
        cout<<"\nERROR***SOLO PUEDE SACAR CITA SI ESTA 'DISPONIBLE'"<<endl;
    }
    cout<<"\n\tRespuesta: ";
    cin>>rp1;
    }while(permiso[rp1-1]==false);
    int z=0;
    switch (rp1)
    {
        case 1:  z=0; break;
        case 2:  z=9;break;
        case 3:  z=18;break;
        case 4:  z=27;break;
        case 5:  z=9*4;break;
        case 6:  z=9*5;break;
        case 7:  z=9*6;break;
        case 8:  z=9*7;break;
        case 9:  z=9*8;break;
        case 10:  z=9*9;break;
    }

//APOYO
    respaldo[posdoc+1+z]=id[0];
    respaldo[posdoc+2+z]=id[1];
    respaldo[posdoc+3+z]=id[2];
    respaldo[posdoc+4+z]=id[3];
    respaldo[posdoc+5+z]=id[4];
    respaldo[posdoc+6+z]=id[5];
    respaldo[posdoc+7+z]=id[6];
    respaldo[posdoc+8+z]=id[7];

    bd[posicion-1]=respaldo;

    ofstream archivo("ArchivosCitas/Citas.csv");
    cout<<"\n\t*****SU CITA SE HA REGISTRADO CON EXITO*****\n\n"<<endl;
    //archivo<<bd.size()<<endl;
    for ( int i =0 ; i<bd.size() ; i++)
    {
        archivo<<bd[i]<<endl;
    }
    system("PAUSE");

    }
}


void MostarCitas()
{
    system("cls");
    cout<<"---------------------------------------------------"<<endl;
    cout<<"******************MOSTRANDO CITAS******************"<<endl;
    cout<<"---------------------------------------------------"<<endl;
    int i=0;//Declaracion dependiente
    string Fecha;
    cout<<"INGRESE LA FECHA (dd/mm): ";
    cin>>Fecha; cout<<""<<endl;
    ifstream ReadFec("ArchivosCitas/Citas.csv");
    string lineatemp;
    string Linea;
    while(getline(ReadFec,lineatemp))
        {
            if (Fecha==lineatemp.substr(5,5))
            {
                Linea=lineatemp;
                ReadFec.close();
            }
        }


    ifstream ReadDoc("ArchivosCitas/SedesEspecialidadesDoctores.csv");

    cout<<"SEDE A LA QUE PERTENECE EL MEDICO ( Lima, San Borja, Piura): ";
    cin>>Fecha;
    system("cls");
    cout<<"-----------------------------------------------------"<<endl;
    cout<<"  ESPECIALIDAD   -   NOMBRE DEL MEDICO    -   CODIGO   "<<endl;
    cout<<"-----------------------------------------------------"<<endl;
    while(getline(ReadDoc,lineatemp))
        {
            if (Fecha==lineatemp.substr(0,Fecha.size()))
            {
                for(int i=0; i<lineatemp.size(); i++)
                {
                    if(lineatemp[i]==';')
                    {
                        lineatemp[i]='-';
                    }
                }
                cout<<lineatemp.substr(Fecha.size()+1,lineatemp.size()-Fecha.size()-1)<<endl;
                i++;
                if (i%10==0 && i>0)
                {
                    cout<<"-----------------------------------------------------------"<<endl;
                    system("PAUSE");
                    cout<<"-----------------------------------------------------------"<<endl;
                }
            }
        }
    cout<<"-----------------------------------------------------------"<<endl;
    cout<<"EL CODIGO DEL DOCTOR A CONSULTAR ES: ";
    cin>>Fecha;
    cout<<"-----------------------------------------------------------"<<endl;
    int Doc=atoi(Fecha.substr(3).c_str());
    //cout<<"\nEL NUMERO DE DOCTOR ES " <<Doc<<endl;
    /***DISPONIBLE=11111111 , NO DISPONIBLE= HC000001 , FUERA DE HORARIO = 22222222 ***/
    Doc=(Doc-1)*92+11;
    Linea=Linea.substr(Doc+1,89);
    //cout<<Linea<<endl;
    string auxn="";
    Horarios.clear();
    for(int i=0 ; i<Linea.size() ; i++)
    {
        if (Linea[i]!=';')
        {
            auxn=auxn+Linea[i];
        }else
        {
            Horarios.push_back(auxn);
            auxn="";
        }
    }
    /****CARGAR NOMBRES*****/
    ifstream ReadUser("usuarios.csv");
    getline(ReadUser,lineatemp);

    vector <string> nombres;
    nombres.clear();
    while(getline(ReadUser,lineatemp))
        {
            string aux6="";
            bool qjk=true;
            lineatemp=lineatemp.substr(9,lineatemp.size()-9);
            for (int i=0 ; i<lineatemp.size() ; i++)
            {
                if(lineatemp[i]==';' && qjk==true)
                {
                    qjk=false;
                    aux6=aux6+" ";
                } else if(lineatemp[i]==';' && qjk==false)
                {
                    i=lineatemp.size();
                }else
                {
                    aux6=aux6+lineatemp[i];
                }
            }
            nombres.push_back(aux6);
        }
    /****/
    Horarios.push_back(auxn);
    float horas=8.00;
    int cont3=0;
    for(int i=0 ; i<Horarios.size() ; i++ )
    {
        cout<<fixed << setprecision(2) << "\t"<<horas<<" - "<<horas+1.00<<" horas: " ;
        horas=horas+1.00;
        if (Horarios[i]=="11111111")
        {
            cout<<"\tSIN CITA PENDIENTE"<<endl;
        }else if (Horarios[i]=="22222222")
        {
            cout<<"\tNO LABORAL"<<endl;
        }else
        {
        	cout<<"\t"<<nombres[atoi(Horarios[i].substr(2).c_str())-1]<<endl;
        }
    }
   system("pause");
}
