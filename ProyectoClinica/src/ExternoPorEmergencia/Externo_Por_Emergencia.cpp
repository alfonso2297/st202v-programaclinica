#include <iostream>
#include <cstdlib>
#include <stdlib.h>
#include <fstream>
#include<sstream>
#include <string.h>
#include <bits/stdc++.h>

using namespace std;
int contador;
vector<string>Horario;

struct emergencia {
    string DNI;
	string FechaAtencion;
    string CamillaDestino;
    string MedicoAsignado;
    string CentroOrigen;
    string FormaIngreso;
};
typedef struct emergencia emergencia;
emergencia aum;
void RegistrarExternoEmergencia(){
    system("cls");
    cout<<"-----------------------------------------------------------"<<endl;
    cout<<"------------REGISTRAR PACIENTE EN EMERGENCIA---------------"<<endl;
    cout<<"-----------------------------------------------------------"<<endl;
    cout<<"\nINGRESE EL NUMERO DE DNI DNI"<<endl;
    cin>>aum.DNI;

    cout<<"\nINGRESE LA FECHA DE INGRESO (dd/mm/aa)"<<endl;
    cin>>aum.FechaAtencion;

    cout<<"\nINGRESE EL CENTRO DE ORIGEN"<<endl;
    int rpt=0;
    do
    {
        cout<<"\t1: LIMA"<<endl;
        cout<<"\t2: SAN BORJA"<<endl;
        cout<<"\t3: PIURA"<<endl;
        cout<<"\t4: VOLVER"<<endl;
        cout<<"\n\tRespuesta: ";
        cin>>rpt;
    }while(rpt!=1 && rpt!=2 && rpt!=3 && rpt!=4);
    switch(rpt){
        case 1: aum.CentroOrigen="Lima"; break;
        case 2: aum.CentroOrigen="San Borja"; break;
        case 3: aum.CentroOrigen="Piura"; break;
        case 4: /** regresar al menu principal*/ break;
    }

    cout<<"\n\tFORMA DE INGRESO"<<endl;
    do
    {
        cout<<"\t1: CAMILLA"<<endl;
        cout<<"\t2: AMBULATORIA"<<endl;
        cout<<"\t3: VOLVER"<<endl;
        cout<<"\n\tRespuesta: ";
        cin>>rpt;
    }while(rpt!=1 && rpt!=2 && rpt!=3);
    switch(rpt){
        case 1: aum.FormaIngreso="Camilla"; break;
        case 2: aum.FormaIngreso="Ambulatoria"; break;
        case 3: /** regresar al menu principal**/ break;
    }

    /**generando el numero aleatorio**/

    int n;
    string res;

        n = 1000 + rand() % (200+contador);
        contador++;

    ostringstream convert;
    convert<<n;
    res=convert.str();
    aum.CamillaDestino = res;

    cout<<"\n\tCODIGO DEL  MEDICO ASIGNADO"<<endl;
    cin>>aum.MedicoAsignado;
}

void MostarExternoEmergencia()
{
    system("cls");
    int i=0;
    string dni;
    cout<<"INGRESE EL NUMERO DE DNI DEL PACIENTE: ";
    cin>>dni; cout<<""<<endl;
    ifstream LeerDni("ExternoEmergencia/ExternosPorEmergencia.csv");
    string lineatemp;
    bool verifica=true;
    string Linea,Sede,FechAtencion,MediAsignado,CamDestino;
    while(getline(LeerDni,lineatemp))
        {
            if (dni==lineatemp.substr(0,8))
            {
                MediAsignado=lineatemp.substr(25,7);
                CamDestino=lineatemp.substr(20,4);
                Linea=lineatemp;
                Sede=lineatemp.substr(33,5);
                FechAtencion=lineatemp.substr(9,10);
                LeerDni.close();
                verifica=false;
            }
        }
    if(!verifica){
    ifstream LeerDatos("ExternoEmergencia/ExternosPorEmergencia.csv");
    cout<<"SEDE A LA QUE PERTENECE EL PACIENTE ES : "<<endl;
    cout<<"\t"<<Sede<<endl;

    cout<<"MEDICO QUE LO ATIENDE : "<<endl;
    cout<<"\t"<<MediAsignado<<endl;

    cout<<"CAMILLA DESTINO : "<<endl;
    cout<<"\t"<<CamDestino<<endl;

    cout<<"FECHA DE ATENCION DEL PACIENTE : "<<endl;
    cout<<"\t"<<FechAtencion<<endl;

    LeerDatos.close();
    }else{
        cout<<"EL PACIENTE CON ESTE DNI NO ESTA REGISTRADO EN ESTE ESTABLECIMIENTO"<<endl;
    }
}

void paciente(){
	int rpta;
	cout<<"----------------------------------------------------------------"<<endl;
	cout<<"---------------------PACIENTES EN EMERGENCIA--------------------"<<endl;
	cout<<"----------------------------------------------------------------"<<endl;
	cout<<" 1 REGISTRAR PACIENTE"<<endl;
	cout<<" 2 MOSTRAR PACIENTE"<<endl;
	cout<<"Ingrese opcion :  ";
	cin>>rpta;

	switch(rpta){
		case 1:{
        	ofstream guardar;
        	guardar.open("ExternoEmergencia/ExternosPorEmergencia.csv",ios::app);
            RegistrarExternoEmergencia();
            guardar<<aum.DNI<<";"<<aum.FechaAtencion<<";"<<aum.CamillaDestino<<";"<<aum.MedicoAsignado<<";"<<aum.CentroOrigen<<";"<<aum.FormaIngreso<<"\n";
            break;
		}
		case 2:{
			MostarExternoEmergencia();
		}

}
 system("pause");

}
