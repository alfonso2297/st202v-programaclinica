#include <vector>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string.h>

using namespace std;
struct serv_ad {
    int codigo;
    string nombre;
    int precio;
    string fecha_oferta;
    string descripcion;
};
typedef struct serv_ad serv_ad;
//Adicionar un registro a la lista
void registrar_act(){
    int c,p;
    string n,d;
    cout<<"------------ADICIONAR-SERVICIO---------------"<<endl;
    cout<<"Ingrese el codigo de la actividad"<<endl;
    cin>>c;
    cout<<"Ingrese el nombre de la actividad"<<endl;
    cin>>n;
    cout<<"Ingrese el precio de la actividad"<<endl;
    cin>>p;
    cout<<"Ingrese la descripcion sobre la actividad"<<endl;
    cin>>d;
    ofstream guardar;
    guardar.open("ServiciosAdicionales/Servicios_adicionales.csv",ios::app);
    guardar<<"\n";
    guardar<<c<<";"<<n<<";"<<p<<";;"<<d;
    guardar.close();
}
void admin_config_servad(){
    int opcion;
    string nomb,fechaof,descrip,Bcod,cod,precio,Nprecio,Nfecha,linea;
        cout<<"-----------CONFIGURACION-DE-SERVICIOS-ADICIONALES----------"<<endl;
        cout<<"1 Adicionar servicio adicional"<<endl;
        cout<<"2 Modificar un servicio"<<endl;
        cout<<"Ingrese opcion"<<endl;
        cin>>opcion;
        switch(opcion){
            case 1: {
                registrar_act();
                break;
            }
            case 2: {
                bool encontrado=false;
                cout<<"---------------MODIFICAR-SERVICIO-ADICIONAL-------------"<<endl;
                cout<<"�Que desea hacer?"<<endl;
                cout<<"1 Modificar precio"<<endl;
                cout<<"2 Crear oferta"<<endl;
                cin>>opcion;
                ifstream leer;
                ofstream temp;
                ifstream aux;
                leer.open("ServiciosAdicionales/Servicios_adicionales.csv");
                temp.open("ServiciosAdicionales/Temp.csv");
                aux.open("ServiciosAdicionales/Servicios_adicionales.csv");
                //Se hace una lectura adicional para conocer 1 posicion antse que acabe el archivo
                switch(opcion){
                    case 1: {
                        cout<<"--------------MODIFICANDO-PRECIO--------------"<<endl;
                        cout<<"Ingrese el codigo de la actividad"<<endl;
                        cin>>Bcod;
                        while(!leer.eof()){
                        	getline(aux,linea);
                            getline(leer,cod,';');
                            getline(leer,nomb,';');
                            getline(leer,precio,';');
                            getline(leer,fechaof,';');
                            getline(leer,descrip,'\n');
                            if(cod==Bcod){
                                encontrado=true;
                                cout<<"Codigo de evento: "<<cod<<endl;
                                cout<<"Nombre del evento: "<<nomb<<endl;
                                cout<<"Precio del evento: "<<precio<<endl;
                                cout<<"Fecha gratuita: "<<fechaof<<endl;
                                cout<<"Descripci�n: "<<descrip<<endl;
                                cout<<endl;
                                cout<<"Ingrese el nuevo precio"<<endl;
                                cin>>Nprecio;
                            //saber si es el fin del archivo
                                if(aux.eof()){
                                    temp<<cod<<";"<<nomb<<";"<<Nprecio<<";"<<fechaof<<";"<<descrip;
                                }else{
                                    temp<<cod<<";"<<nomb<<";"<<Nprecio<<";"<<fechaof<<";"<<descrip<<endl;
                                }
                            }else{
                                if(aux.eof()){
                                temp<<cod<<";"<<nomb<<";"<<precio<<";"<<fechaof<<";"<<descrip;
                                }else{
                                temp<<cod<<";"<<nomb<<";"<<precio<<";"<<fechaof<<";"<<descrip<<endl;
                                }
                            }
                        }
                        break;
                    }
                    case 2: {
                        cout<<"--------------CREANDO-OFERTA----------------"<<endl;
                        cout<<"Ingrese el codigo de la actividad"<<endl;
                        cin>>Bcod;
                        while(!leer.eof()){
                        	getline(aux,linea);
                            getline(leer,cod,';');
                            getline(leer,nomb,';');
                            getline(leer,precio,';');
                            getline(leer,fechaof,';');
                            getline(leer,descrip,'\n');
                            if(cod==Bcod){
                                encontrado=true;
                                cout<<"Codigo de evento: "<<cod<<endl;
                                cout<<"Nombre del evento: "<<nomb<<endl;
                                cout<<"Precio del evento: "<<precio<<endl;
                                cout<<"Fecha gratuita: "<<fechaof<<endl;
                                cout<<"Descripci�n: "<<descrip<<endl;
                                cout<<endl;
                                cout<<"Ingrese la fecha de oferta"<<endl;
                                cin>>Nfecha;
                                //fin del archivo aux
                                if(aux.eof()){
                                    temp<<cod<<";"<<nomb<<";"<<precio<<";"<<Nfecha<<";"<<descrip;
                                }else{
                                    temp<<cod<<";"<<nomb<<";"<<precio<<";"<<Nfecha<<";"<<descrip<<endl;
                                }
                            }else{
                                if(aux.eof()){
                                temp<<cod<<";"<<nomb<<";"<<precio<<";"<<fechaof<<";"<<descrip;
                                }else{
                                temp<<cod<<";"<<nomb<<";"<<precio<<";"<<fechaof<<";"<<descrip<<endl;
                                }
                            }
                        }
                        break;
                    }
                aux.close();
                leer.close();
                temp.close();
                remove("ServiciosAdicionales/Servicios_adicionales.csv");
                rename("ServiciosAdicionales/temp.csv","Servicios_adicionales.csv");
                }
                if(encontrado==false){
                    cout<<"Codigo no encontrado"<<endl;
                }else{
                    cout<<"Modificaci�n satisfactoria"<<endl;
                }
                break;
            }
        }
    }
void admin_mostrar_servad(){
    string c,n,p,f,d,linea;
    ifstream mostrar;
    mostrar.open("ServiciosAdicionales/Servicios_adicionales.csv");
    getline(mostrar,linea);
    while(!mostrar.eof()){
        getline(mostrar,c,';');
        getline(mostrar,n,';');
        getline(mostrar,p,';');
        getline(mostrar,f,';');
        getline(mostrar,d,'\n');
        cout<<"*****************************************************************************************************************"<<endl;
        cout<<"** Codigo: "<<c<<endl;
        cout<<"** Nombre: "<<n<<"\tPrecio: "<<p<<"\tFecha de oferta: "<<f<<endl;
        cout<<"** Descripcion: "<<d<<endl;
    }
    mostrar.close();
}
